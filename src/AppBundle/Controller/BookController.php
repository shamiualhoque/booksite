<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Book;
use AppBundle\Entity\Genre;
use AppBundle\Form\BookType;
use AppBundle\Form\ReviewType;
use AppBundle\Entity\Review;
use AppBundle\Service\BookManager;
use DateTime;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BookController extends Controller
{
    public function indexAction(Request $request)
    {
        $bookManager = $this->container->get('book_manager');

        $books = $bookManager->getAllBooksQuery();

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $books,
            $request->query->getInt('page', 1),
            12
        );

        return $this->render('AppBundle:Room:index.html.twig',
            [
                'pagination' => $pagination,
            ]
        );
    }

    public function genreAction()
    {
        $bookManager = $this->container->get('book_manager');

        $genre = $bookManager->getAllBookGenresWithId();

        return $this->render('AppBundle:Room:genre.html.twig', ['genreArray' => $genre]);
    }

    public function viewAction($id, Request $request)
    {
        $bookManager = $this->getBookManagerContainer();

        $genre = $bookManager->getGenreFromId($id);

        $books = $bookManager->getBooksFromGenre($genre);

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $books,
            $request->query->getInt('page', 1),
            12
        );

        return $this->render('AppBundle:Room:genreView.html.twig',
            [
                'id' => $id,
                'pagination' => $pagination,
                'genre' =>  $genre->getGenre()
            ]
        );
    }

    public function createAction(Request $request)
    {
        $entityManger = $this->getDoctrine()->getManager();
        $bookManager = $this->container->get('book_manager');
        $bookEntry = new Book();

        $form = $this->createForm(BookType::class, $bookEntry,
            ['action' => $request->getUri(), 'book_manager' => $bookManager]);

        $form->handleRequest($request);

        if($form->isValid()) {
            $bookEntry->setTitle($bookEntry->getTitle());
            $bookEntry->setAuthor($bookEntry->getAuthor());
            $bookEntry->setSynopsis($bookEntry->getSynopsis());
            $bookEntry->setIsbn($bookEntry->getIsbn());
            $bookEntry->setPublisher($bookEntry->getPublisher());
            $bookEntry->setFormat($bookEntry->getFormat());
            $bookEntry->setPages($bookEntry->getPages());
            $bookEntry->setPublishDate($bookEntry->getPublishDate());

            $bookEntry->setGenre($bookManager->getGenreFromId($bookEntry->getGenre()));

            /** @var UploadedFile $image */
            $image = $bookEntry->getImage();

            $filename = md5(uniqid()).'.'.$image->guessExtension();

            $image->move(
                $this->getParameter('book_cover_directory'),
                $filename
            );

            $bookEntry->setImage($filename);

            $entityManger->persist($bookEntry);
            $entityManger->flush();

            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Book was successfully added!');

            $url = $this->generateUrl('book_create');

            return $this->redirect($url);
        }

        return $this->render('AppBundle:Room:create.html.twig',
            ['form' => $form->createView()]);
    }

    public function searchBookByTitleAction(Request $request)
    {
        $form = $this->createFormBuilder(null)
            ->add('title', TextType::class)
            ->add('submit', SubmitType::class)
            ->getForm();

        return $this->render(
            'AppBundle:Room:searchBookFromTitle.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function searchBookByISBNAction(Request $request)
    {
        $form = $this->createFormBuilder(null)
            ->add('isbn', TextType::class)
            ->add('submit', SubmitType::class)
            ->getForm();

        return $this->render(
            'AppBundle:Room:searchBookFromISBN.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function resultsFromSearchAction(Request $request)
    {
        $form = $request->request->get('form');

        $bookManager = $this->container->get('book_manager');

        $books = [];

        if (array_key_exists('title', $form)) {
            $title = $form['title'];

            $books = $bookManager->getBooksFromSearchTitle($title);
        }

        if (array_key_exists('isbn', $form)) {
            $isbn = $form['isbn'];

            $books = $bookManager->getBooksFromSearchISBN($isbn);

            return $this->render(
                'AppBundle:Room:viewFromSearch.html.twig',
                [
                    'isbn' => $books['isbn'],
                    'title' => $books['title'],
                    'publishDate' => $books['publishDate'],
                    'publisher' => $books['publisher'],
                    'author' => $books['author'],
                    'description' => $books['description'],
                    'categories' => $books['categories'],
                    'coverURl' => $books['coverUrl']
                ]);
        }

        return $this->render(
            'AppBundle:Room:booksFromSearch.html.twig',
            [
                'books' => $books
            ]);
    }

    public function viewBookFromSearchAction(Request $request)
    {
        $requestQuery = $request->query;

        return $this->render('AppBundle:Room:viewFromSearch.html.twig', [
            'isbn' => $requestQuery->get('isbn'),
            'title' => $requestQuery->get('title'),
            'publishDate' => $requestQuery->get('publishDate'),
            'publisher' => $requestQuery->get('publisher'),
            'author' => $requestQuery->get('author'),
            'description' => $requestQuery->get('description'),
            'categories' => $requestQuery->get('categories'),
            'coverURl' => $requestQuery->get('coverUrl')
        ]);
    }

    public function addBookFromSearchAction(Request $request)
    {
        $requestQuery = $request->query;
        $em = $this->getEntityManager();
        $bookEntry = new Book();
        $bookManager = $this->container->get('book_manager');

        $genre = $this->getEntityManager()->getRepository(Genre::class)->findBy(['genre' => $requestQuery->get('genre')]);

        if (!$genre) {
            $genre = new Genre();
            $genre->setGenre($requestQuery->get('genre'));
            $em->persist($genre);
            $em->flush();
        }

        $bookEntry->setTitle($requestQuery->get('title'));
        $bookEntry->setAuthor($requestQuery->get('author'));
        $bookEntry->setSynopsis($requestQuery->get('synopsis'));
        $bookEntry->setIsbn($requestQuery->get('isbn'));
        $bookEntry->setPublisher($requestQuery->get('publisher'));
        $bookEntry->setPublishDate(new DateTime($requestQuery->get('publishDate')));
        $bookEntry->setGenre($genre);
        $bookEntry->setImage($requestQuery->get('image'));
        $bookEntry->setGoogleApi(true);

        $em->persist($bookEntry);
        $em->flush();

        return $this->render('AppBundle:Room:bookAddedSuccessfully.html.twig');
    }

    public function reviewAction($bookId)
    {
        $bookManager = $this->getBookManagerContainer();

        $bookInfo = $bookManager->getBookFromId($bookId);

        $ratingAndPrice = $bookManager->getRatingandLinkFromGoogle($bookInfo->getIsbn());

        $bookReviews = $bookManager->get10RecentBookReviews($bookId, 5, 0);

        $avgRating = $bookManager->getAverageRatingForBook($bookId);

        return $this->render('AppBundle:Room:bookView.html.twig',
            [
                'bookInfo' => $bookInfo,
                'bookId' => $bookId,
                'reviews' => $bookReviews,
                'avgRating' => $avgRating,
                'googleRating' => $ratingAndPrice['averageRating'],
                'amount' => $ratingAndPrice['amount'],
                'buyingLink' => $ratingAndPrice['buyingLink']
            ]
        );
    }

    public function allReviewsAction($bookId)
    {
        $bookManager = $this->getBookManagerContainer();

        $reviews = $bookManager->getAllBooksReviews($bookId);

        return $this->render('AppBundle:Room:allreviews.html.twig',
            [
                'reviews' => $reviews
            ]
        );
    }

    public function usereviewAction($bookId, Request $request)
    {
        $reviewEntry = new Review();

        $form = $this->createForm(ReviewType::class, $reviewEntry,
            [
                'action' => $request->getUri()
            ]
        );

        $form->handleRequest($request);

        if($form->isValid()) {
            $em = $this->getEntityManager();

            $book = $this->getBookManagerContainer()->getBookFromId($bookId);

            $reviewEntry->setUserId($this->getUser());
            $reviewEntry->setDate(new DateTime('now'));
            $reviewEntry->setRating($reviewEntry->getRating());
            $reviewEntry->setReview($reviewEntry->getReview());
            $reviewEntry->setBookId($book);

            $em->persist($reviewEntry);
            $em->flush($reviewEntry);

            return $this->redirectToRoute('book_review', ['bookId' => $bookId]);
        }

        return $this->render('AppBundle:Room:usereview.html.twig',
            ['form' => $form->createView()]);
    }

    /**
     * @return BookManager|object
     */
    private function getBookManagerContainer()
    {
        return $this->container->get('book_manager');
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return $this->container->get('doctrine.orm.default_entity_manager');
    }
}
