<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PostController extends Controller
{
    public function searchAction()
    {
        $form = $this->createFormBuilder(null)
            ->add('search', TextType::class)
            ->getForm();

        return $this->render('AppBundle:post:search.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @param Request $request
     *
     * @return Response
     */
    public function handleSearchAction(Request $request)
    {
        $search = $request->request->get('form')['search'];

        $searchManager =  $this->container->get('search_manager');

        $books = $searchManager->getBooksFromSearch($search);

        return $this->render('AppBundle:post:view.html.twig',
            [
                'searchString' => $search,
                'books' => $books
            ]
        );
    }

    public function deleteReviewAction(Request $request)
    {
        $reviewId = $request->request->get('id');
        $bookId = $request->request->get('bookId');

        $bookManager = $this->getBookManagerContainer();

        $bookManager->deleteBookReview($reviewId);

        return $this->redirectToRoute('book_all_reviews',
            [
                'bookId' => $bookId
            ]
        );
    }

    /**
     * @return BookManager|object
     */
    private function getBookManagerContainer()
    {
        return $this->container->get('book_manager');
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return $this->container->get('doctrine.orm.default_entity_manager');
    }
}
