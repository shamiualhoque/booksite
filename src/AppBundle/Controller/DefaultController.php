<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $bookManager = $this->container->get('book_manager');

        $bestSellers = $bookManager->NYTimesBestSellers();

        $books = $bookManager->getTop6RecentBooks();

        return $this->render('AppBundle:Default:index.html.twig', [
            'books' => $books,
            'bestSellers' => $bestSellers
        ]);
    }

    public function aboutAction()
    {
        return $this->render('AppBundle:Default:about.html.twig');
    }

    public function apiClientAction()
    {
        if ($this->getUser()) {

            $clientManager = $this->container->get('fos_oauth_server.client_manager.default');
            $client = $clientManager->createClient();
            $client->setRedirectUris(array('localhost'));
            $client->setAllowedGrantTypes(array('token', 'authorization_code'));
            $clientManager->updateClient($client);

            return $this->render('AppBundle:Api:apiClient.html.twig', [
                'client_id' => $client->getPublicId(),
                'client_secret' => $client->getSecret()
            ]);
        }
    }
}
