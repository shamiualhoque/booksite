<?php

namespace AppBundle\Form;

use AppBundle\Service\BookManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BookType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var BookManager $bookManager */
        $bookManager = $options['book_manager'];

        $genreArray = $bookManager->getAllBookGenresWithIdAsValue();

        $builder
            ->add('title')
            ->add('author')
            ->add('synopsis', TextareaType::class)
            ->add('genre', ChoiceType::class, [
                'choices' => [
                    'Genre\'s' => $genreArray
                ]
            ])
            ->add('image', FileType::class, ['data_class' => null])
            ->add('isbn')
            ->add('publisher')
            ->add('publishDate', DateType::class, [
                'widget' => 'choice',
                'years' => range(2018, 1950)
            ])

            ->add('submit', SubmitType::class);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Book'
        ));

        $resolver->setRequired('book_manager');
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'bookreview_bookbundle_book';
    }
}
