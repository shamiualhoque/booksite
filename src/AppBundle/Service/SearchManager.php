<?php

namespace AppBundle\Service;


use AppBundle\Entity\Book;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;

class SearchManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * BookManager constructor.
     *
     * @param $entityManager EntityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getBooksFromSearch($searchResult)
    {
        $sql =
            'SELECT * from book where title LIKE ?'.
                ' UNION '.
                'SELECT * from book where author LIKE ?'.
                ' UNION '.
                'SELECT b.* from book as b INNER JOIN genre as g on b.genre = g.id AND g.genre LIKE ?';

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->bindValue(1, '%'.$searchResult.'%');
        $stmt->bindValue(2, '%'.$searchResult.'%');
        $stmt->bindValue(3, '%'.$searchResult.'%');
        $stmt->execute();

        return $stmt->fetchAll();
    }

//    public function getSearchLikeBookTitle($searchResult)
//    {
//        $result =
//            $this->getEntityManager()
//                ->getRepository(Book::class)->createQueryBuilder('b')
//                    ->where('b.title LIKE :searchResult')
//                    ->setParameter('searchResult', '%' . $searchResult . '%')
//                    ->getQuery();
//
//        return $result;
//
//    }
//
//    /**
//     * @todo date field needs adding to the book database asap.
//     */
//    public function getSearchLikeBookDate()
//    {
//
//    }
//
//    public function getSearchLikeBookAuthor($searchResult)
//    {
//        $result =
//            $this->getEntityManager()
//                ->getRepository(Book::class)->createQueryBuilder('b')
//                ->where('b.author LIKE :searchResult')
//                ->setParameter('searchResult', '%' . $searchResult . '%')
//                ->getQuery();
//
//        return $result;
//    }
//
//    public function getSearchLikeBookGenre($searchResult)
//    {
//        $result =
//            $this->getEntityManager()
//                ->getRepository(Book::class)->createQueryBuilder('b')
//                ->innerJoin('b.genre', 'g')
//                ->where('g.genre LIKE :searchResult')
//                ->setParameter('searchResult', '%' . $searchResult . '%')
//                ->getQuery();
//
//        return $result;
//    }


    /** MORE FUNCTIONS WILL NEED TO BE CREATED IS MORE FIELDS ARE ADDED :) */

    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return $this->entityManager;
    }
}