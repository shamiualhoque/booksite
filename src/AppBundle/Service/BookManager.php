<?php

namespace AppBundle\Service;

use AppBundle\Entity\Book;
use AppBundle\Entity\Review;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;

class BookManager
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * BookManager constructor.
     *
     * @param $entityManager EntityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function getAllBooksQuery()
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery('SELECT b FROM AppBundle:Book b');

        return $query;
    }

    /**
     * Returns all Books genres
     *
     * @return array
     */
    public function getAllBookGenresWithId()
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT g.id, g.genre from AppBundle:Genre g');

        return $query->getResult();
    }

    public function getAllBookGenresWithIdAsValue()
    {
        $em = $this->getEntityManager();

        $result = $em->createQuery(
            'SELECT g.id, g.genre from AppBundle:Genre g'
        )->getResult();

        $genres = array_column($result, 'genre', 'id');

        return $genres;
    }

    public function getGenreFromId($id)
    {
        $em = $this->getEntityManager();

        $result = $em->getRepository(Genre::class)->find($id);

        return $result;
    }

    public function getBooksFromGenre($id)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery(
            'SELECT b FROM AppBundle:Book b WHERE b.genre = :id'
        )->setParameter('id', $id);

        $result = $query;

        return $result;
    }

    public function getBookFromId($id)
    {
        $em = $this->getEntityManager();

        return $em->getRepository(Book::class)->find($id);
    }

    /**
     * @param $bookId
     * @param $limit
     * @param $offset
     *` 
     * @return array
     */
    public function get10RecentBookReviews($bookId, $limit, $offset) {
        $em = $this->getEntityManager();

        $result = $em->getRepository(Review::class)->findBy(
            ['book_id' => $bookId, 'soft_delete' => null],
            ['date' => 'ASC'],
            $limit,
            $offset
        );

        return $result;
    }

    public function getTop6RecentBooks()
    {
        $em = $this->getEntityManager();

        $result = $em->getRepository(Book::class)->findBy(
            [],
            ['date_added' => 'DESC'],
            6
        );

        return $result;
    }

    public function getAllBooksReviews($bookId) {
        $em = $this->getEntityManager();

        $result = $em->getRepository(Review::class)->findBy(
            ['book_id' => $bookId, 'soft_delete' => null],
            ['date' => 'ASC']
        );

        return $result;
    }

    public function deleteBookReview($reviewId)
    {
        $em = $this->getEntityManager();

        $result = $em->createQuery('UPDATE AppBundle:Review r SET r.soft_delete = TRUE WHERE r.id = :review_id')
            ->setParameter('review_id', $reviewId);

        $result->execute();
    }

    public function getAverageRatingForBook($bookId)
    {
        $em = $this->getEntityManager();

        $result = $em->createQuery('SELECT AVG(r.rating) FROM AppBundle:Review r WHERE r.book_id = :bookId')
            ->setParameter('bookId', $bookId);

        return $result->getSingleScalarResult();
    }

    /**
     * @param $isbn
     *
     * @return array|null
     */
    public function getBookFromIsbn($isbn)
    {
        return $this->getGoogleBookWithISBN($isbn);
    }

    public function getBooksFromSearchTitle($searchText)
    {
        return $this->searchGoogleBooks($searchText);
    }

    public function getBooksFromSearchISBN($isbn)
    {
        return $this->searchGoogleBooksByISBN($isbn);
    }

    public function NYTimesBestSellers()
    {
        $client = new Client();

        $response = $client->request('GET', 'https://api.nytimes.com/svc/books/v3/lists/best-sellers/history.json?api-key=af63683ad7b94585a2361e6e7f418fe9');

        $books = [];

        if ($response->getStatusCode() == 200) {

            $responseArray = json_decode($response->getBody(), true);

            if ($responseArray['results']) {
                $results = $responseArray['results'];

                foreach ($results as $bookKey => $bookValue) {
                    if($bookValue['isbns']) {
                    array_push($books, [
                       'title' => $bookValue['title'],
                       'isbn' => $bookValue['isbns'][0]['isbn13'],
                    ]);
                    }
                }
            }
        }

        return $books;
    }

    /**
     * @param $isbn
     *
     * @return array|null
     */
    private function searchGoogleBooks($searchText)
    {
        $api = new Client(['base_uri' => 'https://www.googleapis.com/books/v1/']);
        $response = $api->get('volumes?q=title=' . $searchText);

        $books = [];

        if ($response->getStatusCode() == 200) {
            $match = json_decode((string)$response->getBody(), true);

            if ($match['totalItems'] != 0) {
                foreach ($match["items"] as $marchKey => $marchValue) {
                    foreach ($marchValue as $volumeKey => $volumeValue) {
                        if(is_array($volumeValue)) {
                            if (array_key_exists("industryIdentifiers", $volumeValue)) {

                                array_push($books, [
                                    "isbn" => $volumeValue['industryIdentifiers'][0]['identifier'],
                                    "title" => $volumeValue["title"],
                                    "publishDate" => $volumeValue["publishedDate"],
                                    "publisher" => array_key_exists("publisher", $volumeValue) ? $volumeValue["publisher"] : "Unknown",
                                    "author" => $volumeValue["authors"] ? $volumeValue["authors"][0] : "Unknown",
                                    "description" => array_key_exists("description", $volumeValue) ? $volumeValue["description"] : "No description.",
                                    "categories" => array_key_exists('categories', $volumeValue) ? $volumeValue['categories'][0] : "Unknown",
                                    "coverUrl" => $volumeValue["imageLinks"] ? $volumeValue["imageLinks"]["thumbnail"] : 'blank.png'
                                ]);
                            }
                        }
                    }
                }
            }
        }

        return $books;
    }

    /**
     * @param $isbn
     *
     * @return array|null
     */
    private function searchGoogleBooksByISBN($isbn)
    {
        $api = new Client(['base_uri' => 'https://www.googleapis.com/books/v1/']);
        $response = $api->get('volumes?q=isbn=' . $isbn);

        $books = [];

        if ($response->getStatusCode() == 200) {
            $match = json_decode((string)$response->getBody(), true);

            if ($match['totalItems'] != 0) {
                foreach ($match["items"] as $marchKey => $marchValue) {
                    foreach ($marchValue as $volumeKey => $volumeValue) {
                        if(is_array($volumeValue)) {
                            if (array_key_exists("industryIdentifiers", $volumeValue)) {

                                array_push($books, [
                                    "isbn" => $volumeValue['industryIdentifiers'][0]['identifier'],
                                    "title" => $volumeValue["title"],
                                    "publishDate" => $volumeValue["publishedDate"],
                                    "publisher" => array_key_exists("publisher", $volumeValue) ? $volumeValue["publisher"] : "Unknown",
                                    "author" => $volumeValue["authors"] ? $volumeValue["authors"][0] : "Unknown",
                                    "description" => array_key_exists("description", $volumeValue) ? $volumeValue["description"] : "No description.",
                                    "categories" => array_key_exists('categories', $volumeValue) ? $volumeValue['categories'][0] : "Unknown",
                                    "coverUrl" => $volumeValue["imageLinks"] ? $volumeValue["imageLinks"]["thumbnail"] : 'blank.png'
                                ]);
                            }
                        }
                    }
                }
            }
        }

        // returns one book
        return $books[0];
    }

    /**
     * @param $isbn
     *
     * @return array|null
     */
    public function getRatingandLinkFromGoogle($isbn)
    {
        $api = new Client(['base_uri' => 'https://www.googleapis.com/books/v1/']);
        $response = $api->get('volumes?q=isbn=' . $isbn);

        $ratingAndPrice = [];

        if ($response->getStatusCode() == 200) {
            $match = json_decode((string)$response->getBody(), true);

            if ($match['totalItems'] != 0) {

                $bookItem = $match['items'][0];

                $amount = 'UNKNOWN';
                $buyingLink = 'UNKNOWN';
                $averageRating = 0;

                if(is_array($bookItem['saleInfo'])) {
                    $saleInfo = $bookItem['saleInfo'];

                    if (in_array('listPrice', $saleInfo)) {
                        $amount = array_key_exists(
                            'amount',
                            $saleInfo['listPrice']
                        ) ? $saleInfo['listPrice']['amount'] : "UNKNOWN";
                        $buyingLink = array_key_exists('buyLink', $saleInfo) ? $saleInfo['buyLink'] : "UNKNOWN";
                    }
                }

                if(is_array($bookItem['volumeInfo'])) {
                    $bookInfo = $bookItem['volumeInfo'];

                    $averageRating = array_key_exists('averageRating', $bookInfo) ? $bookInfo['averageRating'] : "0";
                }

                $ratingAndPrice = [
                       'amount' => $amount,
                       'buyingLink' => $buyingLink,
                       'averageRating' => $averageRating
                    ];
            }
        }

        return $ratingAndPrice;
    }

    /**
     * @return EntityManager
     */
    private function getEntityManager()
    {
        return $this->entityManager;
    }
}