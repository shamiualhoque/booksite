<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180508154203 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE oauth2_clients ADD user_id_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE oauth2_clients ADD CONSTRAINT FK_F9D02AE69D86650F FOREIGN KEY (user_id_id) REFERENCES `user` (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F9D02AE69D86650F ON oauth2_clients (user_id_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE oauth2_clients DROP FOREIGN KEY FK_F9D02AE69D86650F');
        $this->addSql('DROP INDEX UNIQ_F9D02AE69D86650F ON oauth2_clients');
        $this->addSql('ALTER TABLE oauth2_clients DROP user_id_id');
    }
}
