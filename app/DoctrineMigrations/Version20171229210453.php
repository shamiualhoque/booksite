<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171229210453 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Science fiction")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Drama")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Action and Adventure")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Romance")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Mystery")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Horror")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Guide")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Travel")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Children\'s")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Science")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("History")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Poetry")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Comics")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Art")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Fantasy")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Autobiographies")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Biographies")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Series")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Religion, Spirituality & New Age")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Cookbooks")');
        $this->addSql('INSERT INTO `genre` (genre) VALUES ("Manga")');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
